from book.models import Book, UserGroup, UserGroupResAttr
from rest_framework import serializers


class DynamicFieldsSerializerMixin(object):

    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)

        super(DynamicFieldsSerializerMixin, self).__init__(*args, **kwargs)

        if fields is not None:
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class BookSerializer(DynamicFieldsSerializerMixin, serializers.HyperlinkedModelSerializer):
    id = serializers.Field()
    class Meta:
        model = Book
        fields = ('Name', 'Title',  'Author', 'Descripition', 'Price', 'id')


class UserGroupResAttrSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.Field()
    class Meta:
        model = UserGroupResAttr
        fields = ('Column_name', 'Is_active', 'id')
        read_only_fields = ('Column_name',)


class UserGroupSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.Field()
    Attr = UserGroupResAttrSerializer(source='Attr', many=True)
    User_group = serializers.RelatedField('User_group')
    class Meta:
        model = UserGroup
        fields = ('Title', 'User_group',  'Attr', 'id')

