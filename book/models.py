# coding=utf-8
from django.db import models
from django.contrib.auth.models import User, Group


class UserGroupResAttr(models.Model):
    Column_name = models.CharField(max_length=255)
    Is_active = models.BooleanField(default=False)
    id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return self.Column_name

class UserGroup(models.Model):
    Title = models.CharField(max_length=255)
    User_group = models.OneToOneField('auth.Group', null=True, related_name='profile_group')
    Attr = models.ManyToManyField(UserGroupResAttr, related_name='user_group', verbose_name=u'Атрибуты', null=True, blank=True)
    id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return self.Title


class Book(models.Model):
    Name = models.CharField(max_length=20, verbose_name=u'Название')
    Title = models.CharField(max_length=30, verbose_name=u'Заголовок', null=True, blank=True)
    Author = models.CharField(max_length=30, verbose_name=u'Автор')
    Descripition = models.CharField(max_length=512, verbose_name=u'Описание', null=True, blank=True)
    Price = models.IntegerField(default=0, max_length=5, verbose_name=u'Цена')
    id = models.AutoField(primary_key=True)

    def __unicode__(self):
        return self.Name
