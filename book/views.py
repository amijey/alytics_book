from book.models import Book, UserGroup, UserGroupResAttr
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response
from book.serializers import BookSerializer, UserGroupSerializer, UserGroupResAttrSerializer
from django.shortcuts import render
from django.template import Context, loader, RequestContext

@api_view(['GET'])
def api_root(request, format=None):
    context = {}
    return render(request,'index.html', context)


class DynamicFieldsViewMixin(object):

 def get_serializer(self, *args, **kwargs):

    serializer_class = self.get_serializer_class()

    fields = None
    if self.request.method == 'GET':
        usergroups_id = self.request.GET.get("usergroups_id", None)

        if usergroups_id:
            if UserGroupResAttr.objects.filter(user_group__id=usergroups_id).exists():
                fields = [key['Column_name'] for key in list(UserGroupResAttr.objects.filter(user_group__id=usergroups_id, Is_active=True).values('Column_name'))]
                if not 'id' in fields:
                    fields.append('id')
                fields = tuple(fields)

    kwargs['context'] = self.get_serializer_context()
    kwargs['fields'] = fields

    return serializer_class(*args, **kwargs)



class UserGroupList(generics.ListCreateAPIView):
    model = UserGroup
    serializer_class = UserGroupSerializer


class UserGroupResAttrDetail(generics.RetrieveUpdateDestroyAPIView):
    model = UserGroupResAttr
    serializer_class = UserGroupResAttrSerializer


class BookList(DynamicFieldsViewMixin, generics.ListCreateAPIView):
    model = Book
    serializer_class = BookSerializer

class BookDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Book
    serializer_class = BookSerializer
