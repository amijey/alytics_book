# coding=utf-8
from django.contrib import admin
from django.conf import settings
from book.models import *


class ModelAdmin(admin.ModelAdmin):
    actions = ['custom_delete_selected']

    def custom_delete_selected(modeladmin, request, queryset):
        for obj in queryset:
            obj.delete()

    def get_actions(self, request):
        actions = super(ModelAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def delete_model(self, request, obj):
        penalize(obj, request.user)
        obj.delete()

    custom_delete_selected.short_description = u"Удалить выбранные объекты"




class UserGroupAdmin(admin.ModelAdmin):
    fields = ('Title', 'User_group', 'Attr')

class UserGroupResAttrAdmin(admin.ModelAdmin):
    fields = ('Column_name', 'Is_active')

class BookAdmin(admin.ModelAdmin):
    fields = ('Name', 'Title', 'Author', 'Descripition', 'Price')

admin.site.register(Book, BookAdmin)
admin.site.register(UserGroup, UserGroupAdmin)
admin.site.register(UserGroupResAttr, UserGroupResAttrAdmin)
