from django.conf.urls import patterns, url, include
from rest_framework.urlpatterns import format_suffix_patterns
from book import views
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('book.views',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'api_root'),
    url(r'^usergroups/$', views.UserGroupList.as_view(), name='usergroup-list'),
    url(r'^usergroups/attr/(?P<pk>\d+)/$', views.UserGroupResAttrDetail.as_view(), name='usergroup-attr-detail'),
    url(r'^books/$', views.BookList.as_view(), name='book-list'),
    url(r'^books/(?P<pk>\d+)/$', views.BookDetail.as_view(), name='book-detail'),
)

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])
