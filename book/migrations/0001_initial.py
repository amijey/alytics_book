# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Book'
        db.create_table(u'book_book', (
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Author', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Status', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'book', ['Book'])


    def backwards(self, orm):
        # Deleting model 'Book'
        db.delete_table(u'book_book')


    models = {
        u'book.book': {
            'Author': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'object_name': 'Book'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['book']