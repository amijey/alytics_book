# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Book.Status'
        db.delete_column(u'book_book', 'Status')

        # Adding field 'Book.Title'
        db.add_column(u'book_book', 'Title',
                      self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Book.Descripition'
        db.add_column(u'book_book', 'Descripition',
                      self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Book.Price'
        db.add_column(u'book_book', 'Price',
                      self.gf('django.db.models.fields.IntegerField')(default=0, max_length=5),
                      keep_default=False)


        # Changing field 'Book.Name'
        db.alter_column(u'book_book', 'Name', self.gf('django.db.models.fields.CharField')(max_length=20))

        # Changing field 'Book.Author'
        db.alter_column(u'book_book', 'Author', self.gf('django.db.models.fields.CharField')(max_length=30))

    def backwards(self, orm):
        # Adding field 'Book.Status'
        db.add_column(u'book_book', 'Status',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Deleting field 'Book.Title'
        db.delete_column(u'book_book', 'Title')

        # Deleting field 'Book.Descripition'
        db.delete_column(u'book_book', 'Descripition')

        # Deleting field 'Book.Price'
        db.delete_column(u'book_book', 'Price')


        # Changing field 'Book.Name'
        db.alter_column(u'book_book', 'Name', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Changing field 'Book.Author'
        db.alter_column(u'book_book', 'Author', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        u'book.book': {
            'Author': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Descripition': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'Book'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Price': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '5'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['book']