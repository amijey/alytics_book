# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserGroupResAttr'
        db.create_table(u'book_usergroupresattr', (
            ('User_group', self.gf('django.db.models.fields.related.ForeignKey')(related_name='attr', to=orm['book.UserGroup'])),
            ('Column_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('Is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'book', ['UserGroupResAttr'])


    def backwards(self, orm):
        # Deleting model 'UserGroupResAttr'
        db.delete_table(u'book_usergroupresattr')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'book.book': {
            'Author': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Descripition': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'Book'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Price': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '5'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'book.usergroup': {
            'Meta': {'object_name': 'UserGroup'},
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'User_group': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'profile_group'", 'unique': 'True', 'null': 'True', 'to': u"orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'book.usergroupresattr': {
            'Column_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'Is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Meta': {'object_name': 'UserGroupResAttr'},
            'User_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'attr'", 'to': u"orm['book.UserGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['book']