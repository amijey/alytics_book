toggleArr = (array, value) ->
  index = array.indexOf(value)
  if index == -1
    array.push value
  else
    array.splice index, 1
  return

$.urlParam = (name) ->
  results = new RegExp('[?&]' + name + '=([^&#]*)').exec(window.location.href)
  if results then results[1] else 0

$.ajaxPrefilter (options, originalOptions, jqXHR) ->
  options.url = 'http://178.62.144.223' + options.url
  return

$.fn.serializeObject = ->
  o = {}
  a = @serializeArray()
  $.each a, ->
    if o[@name] != undefined
      if !o[@name].push
        o[@name] = [ o[@name] ]
      o[@name].push @value or ''
    else
      o[@name] = @value or ''
    return
  o

Books = Backbone.Collection.extend(
  initialize: (models, options) ->
    @usergroups_id = if options.usergroups_id then options.usergroups_id else 0
    return
  url: ->
    if @usergroups_id then '/books/?usergroups_id=' + @usergroups_id else '/books/'
)

UserGroups = Backbone.Collection.extend(url: '/usergroups/')
Book = Backbone.Model.extend(urlRoot: '/books/')
UserGroupAttr = Backbone.Model.extend(urlRoot: '/usergroups/attr/')
BookList = Backbone.View.extend(
  el: '.page'
  render: ->
    that = this
    that.books = new Books([], usergroups_id: $.urlParam('usergroups_id'))
    that.ugroups = new UserGroups
    that.checkedItems = []
    $.when(that.books.fetch(), that.ugroups.fetch()).done ->
      template = _.template($('#book-list-template').html(),
        books: that.books.models
        ugroups: that.ugroups.models)
      that.$el.html template
      return
    this
  events:
    'click .spoiler-head': 'spoilerMe'
    'click .deleteItems': 'deleteBooks'
    'change .delete-checkbox': 'changeCheck'
  changeCheck: (ev) ->
    toggleArr @checkedItems, parseInt($(ev.target).attr('book-id'))
    false
  spoilerMe: (ev) ->
    $(ev.target).next('.spoiler-body').toggle '500'
    false
  deleteBooks: (ev) ->
    that = this
    _.each _.clone(that.books.models), (model) ->
      if ! !($.inArray(model.get('id'), that.checkedItems) + 1)
        model.destroy
          url: '/books/' + model.get('id')
          success: ->
            that.render()
            return
      return
    false
)
EditBook = Backbone.View.extend(
  el: '.page'
  render: (options) ->
    that = this
    if options.id
      that.book = new Book(id: options.id)
      that.book.fetch success: (book) ->
        template = _.template($('#edit-book-template').html(), book: book)
        that.$el.html template
        return
    else
      template = _.template($('#edit-book-template').html(), book: null)
      @$el.html template
    return
  events:
    'submit .edit-book-form': 'saveBook'
    'click .delete': 'deleteBook'
  saveBook: (ev) ->
    $('.alert-warning').addClass 'hide'
    bookDetails = $(ev.currentTarget).serializeObject()
    book = new Book
    book.save bookDetails,
      success: ->
        router.navigate '', trigger: true
        $('.alert-warning p').remove()
        return
      error: (model, response) ->
        $('.alert-warning').removeClass 'hide'
        $.each JSON.parse(response.responseText), (k, v) ->
          $('.alert-warning').append '<p><b>' + k + ': </b>' + v + '</p>'
          return
        return
    false
  deleteBook: (ev) ->
    @book.destroy success: ->
      router.navigate '', trigger: true
      return
    false
)
EditUGroupAttr = Backbone.View.extend(
  el: '.page'
  render: (options) ->
    that = this
    if options.id
      that.user_groups_attr = new UserGroupAttr(id: options.id)
      that.user_groups_attr.fetch success: (attr) ->
        template = _.template($('#edit-ugroup-attr-template').html(), attr: attr)
        that.$el.html template
        return
    else
      template = _.template($('#edit-ugroup-attr-template').html(), attr: null)
      @$el.html template
    return
  events:
    'submit .edit-ugroup-attr-form': 'saveAttr'
    'click .delete': 'deleteAttr'
  saveAttr: (ev) ->
    userGroupAttrDetails = $(ev.currentTarget).serializeObject()
    user_groups_attr = new UserGroupAttr
    user_groups_attr.save userGroupAttrDetails, success: ->
      router.navigate '', trigger: true
      return
    false
  deleteAttr: (ev) ->
    @user_groups_attr.destroy success: ->
      router.navigate '', trigger: true
      return
    false
)
Router = Backbone.Router.extend(routes:
  '': 'home'
  'add': 'editBook'
  'edit/:id': 'editBook'
  'usergroups/edit/:id': 'editUGroupAttr')
booklist = new BookList
editBook = new EditBook
editUGroupAttr = new EditUGroupAttr
router = new Router
router.on 'route:home', ->
  booklist.render()
  return
router.on 'route:editBook', (id) ->
  editBook.render id: id
  return
router.on 'route:editUGroupAttr', (id) ->
  editUGroupAttr.render id: id
  return
Backbone.history.start()
