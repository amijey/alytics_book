function toggleArr(array, value) {
    var index = array.indexOf(value);

    if (index === -1) {
        array.push(value);
    } else {
        array.splice(index, 1);
    }
}

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results ? results[1] : 0;
}
$.ajaxPrefilter( function( options, originalOptions, jqXHR ) {
    options.url = 'http://178.62.144.223' + options.url;
});

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var Books = Backbone.Collection.extend({
    initialize: function(models, options) {
        this.usergroups_id = options.usergroups_id ? options.usergroups_id : 0;
    },
    url: function() {
        return this.usergroups_id ? '/books/?usergroups_id=' + this.usergroups_id : '/books/';
    }
});

var UserGroups = Backbone.Collection.extend({
    url: '/usergroups/'
});

var Book = Backbone.Model.extend({
    urlRoot: '/books/'
});

var UserGroupAttr = Backbone.Model.extend({
    urlRoot: '/usergroups/attr/'
});


var BookList = Backbone.View.extend({
    el: '.page',
    
    render: function () {

        var that = this;
        that.books = new Books([], { usergroups_id: $.urlParam('usergroups_id') });
        that.ugroups = new UserGroups();
        that.checkedItems = [];


        $.when(that.books.fetch(), that.ugroups.fetch()).done(function() {
            var template = _.template($('#book-list-template').html(), {
                books: that.books.models,
                ugroups: that.ugroups.models
            });
            that.$el.html(template);
        });
        return this;
    },
    events: {
        'click .spoiler-head': 'spoilerMe',
        'click .deleteItems': 'deleteBooks',
        'change .delete-checkbox': 'changeCheck'
    },
    changeCheck: function(ev) {
        toggleArr(this.checkedItems, parseInt($(ev.target).attr('book-id')));
        return false;
    },
    spoilerMe: function(ev) {
        $(ev.target).next('.spoiler-body').toggle('500');
        return false;
    },
    deleteBooks: function(ev) {
        var that = this;
        _.each(_.clone(that.books.models), function(model) {
            if (!!($.inArray( model.get('id'), that.checkedItems ) + 1 ))
                model.destroy({
                    url: "/books/" + model.get('id'),
                    success: function() {
                        that.render();
                    }
                });
        });
    
        return false;
    }

});

var EditBook = Backbone.View.extend({
    el:'.page',
    render: function (options) {
        var that = this;
        if (options.id) {
            that.book = new Book({id: options.id});
            that.book.fetch({
                success: function(book) {
                    
                    var template = _.template($('#edit-book-template').html(), {book: book});
                    that.$el.html(template);
                }
            });
        } else {
            var template = _.template($('#edit-book-template').html(), {book: null});
            this.$el.html(template);
        }
    },
    events: {
        'submit .edit-book-form': 'saveBook',
        'click .delete': 'deleteBook'
    },
    saveBook: function(ev) {
        $('.alert-warning').addClass('hide');

        var bookDetails = $(ev.currentTarget).serializeObject();
        var book = new Book();
        book.save(bookDetails, {
            success: function () {
                router.navigate('', {trigger:true});
                $('.alert-warning p').remove();
            },
            error: function (model, response) {
                $('.alert-warning').removeClass('hide');
                $.each(JSON.parse(response.responseText), function(k,v) {
                    $('.alert-warning').append("<p><b>" + k + ": </b>" + v + "</p>");
                });
            }
        });
        return false;
    },
    deleteBook: function(ev) {
        this.book.destroy({
            success: function() {
                router.navigate('', {trigger:true});
            }
        });
        return false;
    }
});


var EditUGroupAttr = Backbone.View.extend({
    el:'.page',
    render: function (options) {
        var that = this;
        if(options.id) {
            that.user_groups_attr = new UserGroupAttr({id: options.id});
            that.user_groups_attr.fetch({
                success: function(attr) {
                    
                    var template = _.template($('#edit-ugroup-attr-template').html(), {attr: attr});
                    that.$el.html(template);
                }
            });
        } else {
            var template = _.template($('#edit-ugroup-attr-template').html(), {attr: null});
            this.$el.html(template);
        }
    },
    events: {
        'submit .edit-ugroup-attr-form': 'saveAttr',
        'click .delete': 'deleteAttr'
    },
    saveAttr: function(ev) {
        var userGroupAttrDetails = $(ev.currentTarget).serializeObject();
        var user_groups_attr = new UserGroupAttr();
        user_groups_attr.save(userGroupAttrDetails, {
            success: function () {
                router.navigate('', {trigger:true});
            }
        });
        return false;
    },
    deleteAttr: function(ev) {
        this.user_groups_attr.destroy({
            success: function() {
                router.navigate('', {trigger:true});
            }
        });
        return false;
    }
});

var Router = Backbone.Router.extend({
    routes: {
        '': 'home',
        'add': 'editBook',
        'edit/:id': 'editBook',
        'usergroups/edit/:id': 'editUGroupAttr'
    }
});


var booklist = new BookList();
var editBook = new EditBook();
var editUGroupAttr = new EditUGroupAttr();

var router = new Router();

router.on('route:home', function () {
    booklist.render();
});

router.on('route:editBook', function (id) {
    editBook.render({id: id});
});

router.on('route:editUGroupAttr', function (id) {
    editUGroupAttr.render({id: id});
});

Backbone.history.start();
